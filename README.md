Selenium Test for My
====================


Install Selenium
----------------

For example on ubuntu as root:

```bash
npm install -g selenium-side-runner
apt-get install firefox-geckodriver

```

Install My
----------

Check here: https://github.com/genouest/genouestaccountmanager

Or for example:

```bash
git clone git@github.com:FanchTheSystem/genouestaccountmanager.git # or any fork to be tested
cd genouestaccountmanager
git checkout -b find_ldap_cn_from_uid_clean # or any branch to be tested
docker build -t osallou/my . # yes you need docker
npm install # yes you need npm
cd tests
docker-compose up -d # yes you need docker-compose
```

Or use ansible: https://gitlab.cluster.france-bioinformatique.fr/taskforce/ansible-my

Run Selenium Test
-----------------

```bash
selenium-side-runner -c "browserName=firefox"  Test-My-Create-User-Group-And-Change-Password.side
```

If your installation is not on Localhost:

```bash
selenium-side-runner --base-url "http://192.168.105.166:3000" -c "browserName=firefox"  Test-My-Create-User-Group-And-Change-Password.side
```


License
-------

[License](LICENSE)
