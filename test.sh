#!/bin/bash

set -xe

# apt-get install firefox-geckodriver
# apt-get install chromium-chromedriver
# npm install -g selenium-side-runner
# https://selenium.dev/selenium-ide/docs/en/introduction/command-line-runner

preprod_url="https://192.168.103.171"
dev_url="https://192.168.105.165"
preprod_test=$(ls Test-*)
dev_test=$(ls Test-* )
browser="firefox"

for stest in $dev_test
do
    echo "$stest"
   # sleep 30
   # selenium-side-runner --debug --base-url "$dev_url" -c "browserName=$browser acceptInsecureCerts=true" "$stest"
done

for stest in $preprod_test
do
    echo "$stest"
    sleep 10
    selenium-side-runner --debug --base-url "$preprod_url" -c "browserName=$browser acceptInsecureCerts=true" "$stest"
done
